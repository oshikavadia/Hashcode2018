package com.dkit;

import java.util.Comparator;

/**
 * Created by minht on 3/1/2018.
 */
public class DistanceComparator implements Comparator<Ride> {
    @Override
    public int compare(Ride o1, Ride o2) {
        return (o1.getDistance()<o2.getDistance()) ? -1: ((o1.getDistance()==o2.getDistance()? 0:1));
    }
}
