package com.dkit;

public class Ride implements Comparable<Ride>
{
    private static int COUNT = 0;
    private int rideID;
    private int startIntersectionRow;
    private int startIntersectionCol;
    private int finishIntersectionRow;
    private int finishIntersectionCol;
    private int earliestStart;
    private int latestFinish;
    private int distance;
    private boolean isAssigned;

    // Constructor
    public Ride(int a,int b,int x,int y,int s,int f)
    {
        this.startIntersectionRow = a;
        this.startIntersectionCol = b;
        this.finishIntersectionRow = x;
        this.finishIntersectionCol = y;
        this.earliestStart = s;
        this.latestFinish = f;
        this.distance = Math.abs(a-x) + Math.abs(b-y);
        this.rideID = COUNT ++;
        this.isAssigned = false;
    }

    public boolean isAssigned() {
        return isAssigned;
    }

    public void setAssigned(boolean assigned) {
        isAssigned = assigned;
    }

    // Getters
    public int getRideID() {
        return rideID;
    }

    public int getStartIntersectionRow() {
        return startIntersectionRow;
    }

    public int getStartIntersectionCol() {
        return startIntersectionCol;
    }

    public int getFinishIntersectionRow() {
        return finishIntersectionRow;
    }

    public int getFinishIntersectionCol() {
        return finishIntersectionCol;
    }

    public int getEarliestStart() {
        return earliestStart;
    }

    public int getLatestFinish() {
        return latestFinish;
    }

    public int getDistance() {
        return distance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ride)) return false;

        Ride ride = (Ride) o;

        if (getRideID() != ride.getRideID()) return false;
        if (getStartIntersectionRow() != ride.getStartIntersectionRow()) return false;
        if (getStartIntersectionCol() != ride.getStartIntersectionCol()) return false;
        if (getFinishIntersectionRow() != ride.getFinishIntersectionRow()) return false;
        if (getFinishIntersectionCol() != ride.getFinishIntersectionCol()) return false;
        if (getEarliestStart() != ride.getEarliestStart()) return false;
        if (getLatestFinish() != ride.getLatestFinish()) return false;
        return getDistance() == ride.getDistance();
    }

    @Override
    public int hashCode() {
        int result = getRideID();
        result = 31 * result + getStartIntersectionRow();
        result = 31 * result + getStartIntersectionCol();
        result = 31 * result + getFinishIntersectionRow();
        result = 31 * result + getFinishIntersectionCol();
        result = 31 * result + getEarliestStart();
        result = 31 * result + getLatestFinish();
        result = 31 * result + getDistance();
        return result;
    }

//    shit to use Collection.sort

    @Override
    public int compareTo(Ride o) {
        return Integer.compare(this.distance, o.distance);
    }
}