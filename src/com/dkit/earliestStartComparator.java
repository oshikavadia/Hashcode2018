package com.dkit;

import java.util.Comparator;

/**
 * Created by Oshi on 01-Mar-18.
 */
public class earliestStartComparator implements Comparator<Ride> {
    @Override
    public int compare(Ride o1, Ride o2) {
        return Integer.compare(o1.getEarliestStart(),o2.getEarliestStart());
    }
}
