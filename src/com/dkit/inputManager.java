package com.dkit;

import java.util.ArrayList;

/**
 * Created by Oshi on 01-Mar-18.
 */
public class inputManager {
    public static class problemVariables {
        static public int rows;
        static public int columns;
        static public int vehicles;
        static public int numOfRides;
        static public int bonus;
        static public int timeSteps;
    }


    private ArrayList<int[]> lines = new ArrayList<>();
    private ArrayList<Ride> rides = new ArrayList<>();
    inputManager(ArrayList<int[]> lines) {
        this.lines = lines;
        setVariables();
        setRides();
    }

    private void setRides(){
        for (int i = 1 ; i <lines.size();i++){
            int[] line = lines.get(i);
            rides.add(new Ride(line[0],line[1],line[2],line[3],line[4],line[5]));
        }
    }

    private void setVariables(){

        problemVariables.rows = (lines.get(0)[0]);
        problemVariables.columns = (lines.get(0)[1]);
        problemVariables.vehicles = (lines.get(0)[2]);
        problemVariables.numOfRides = (lines.get(0)[3]);
        problemVariables.bonus = (lines.get(0)[4]);
        problemVariables.timeSteps = (lines.get(0)[5]);

    }

    public ArrayList<Ride> getRides(){
        return this.rides;
    }
}
